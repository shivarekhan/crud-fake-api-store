import React from "react";
import axios from "axios";
import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";
import Navbar from "./Components/Navbar/Navbar";
import ShowAllProducts from "./Components/ShowAllProducts/ShowAllProducts";
import Admin from "./Components/Admin/Admin";
import AddNewProduct from "./Components/AddNewProduct/AddNewProduct";

class App extends React.Component {
  constructor(props) {
    super(props)
    this.API_STATUS = {
      LOADED: "loaded",
      LOADING: "loading",
      ERROR: "error"
    }

    this.state = {
      productList: [],
      errorMsg: "",
      apiStatus: this.API_STATUS.LOADING,
    }

    this.API_ENDPOINT = "https://fakestoreapi.com/products/";

  }

  addNewProduct = (receivedProduct) => {
    let products = this.state.productList;
    let length = products.length;
    let lastIndex = products[length - 1].id;

    let newProduct = {
      "id": lastIndex + 1,
      "title": receivedProduct.title,
      "price": receivedProduct.price,
      "description": receivedProduct.description,
      "category": receivedProduct.category,
      "image": receivedProduct.image,
      "rating": { "rate": 0, "count": 0 }
    }

    this.setState({
      productList: [...products, newProduct]
    }, () => {
      console.log("product added")
    })

  }

  removeProduct = (idForProductToRemoved) => {
    let products = this.state.productList;
    let productToDelete = products.filter((current) => {
      return current.id !== idForProductToRemoved;
    })
    this.setState({
      productList: productToDelete,
    })
  }

  updateProduct = (receivedProduct) => {
    let newProduct = {
      "id": receivedProduct.id,
      "title": receivedProduct.title,
      "price": receivedProduct.price,
      "description": receivedProduct.description,
      "category": receivedProduct.category,
      "image": receivedProduct.image,
      "rating": { "rate": receivedProduct.rating, "count": receivedProduct.count }
    }
    let products = this.state.productList.map((current) => {
      if (current.id === receivedProduct.id) {
        return newProduct;
      } else {
        return current;
      }

    });
  
    this.setState({
      productList: products,
    })

  }

  componentDidMount() {

    this.setState({
      status: this.API_STATUS.LOADING,
    }, () => {

      axios.get(this.API_ENDPOINT)
        .then((response) => {
          this.setState({
            apiStatus: this.API_STATUS.LOADED,
            productList: response.data,
            errorMsg: "",
          })
        })
        .catch(() => {
          this.setState({
            apiStatus: this.API_STATUS.ERROR,
            errorMsg: "NO products found please try after some time"
          })
        })
    })
  }

  render() {
    return (
      <>
        <BrowserRouter>
          <Navbar />
          <Switch>

            <Route exact path="/">
              <ShowAllProducts
                products={this.state.productList}
                status={this.state.apiStatus}
                errorMsg={this.state.errorMsg}
              />
            </Route>

            <Route exact path="/admin">
              <Admin
                status={this.state.apiStatus}
                products={this.state.productList}
                removeProduct={this.removeProduct}
                updateProduct={this.updateProduct}
              />
            </Route>

            <Route exact path="/addNewProduct">
              <AddNewProduct
                addNewProduct={this.addNewProduct} />
            </Route>

          </Switch>
        </BrowserRouter>


      </>

    )
  }

}

export default App;