import React from 'react'
import { Link } from 'react-router-dom'
import "../Admin/Admin.css"
import Loader from '../Loader/Loader'
import ProductEntry from '../ProductEntry/ProductEntry'

class Admin extends React.Component {
    render() {
        return (
            <>
                {this.props.status === "loading" && <Loader />}
                <div className="container-xl">
                    <div className="table-responsive">
                        <div className="table-wrapper">
                            <div className="table-title ">
                                <div className="row">
                                    <div className="col-sm-10">
                                        <h2>Manage <b>Product</b></h2>
                                    </div>
                                    <div className="col-sm-2 ">
                                        <Link to="/addNewProduct">
                                            <button className='btn-addNew'>Add New Product</button></Link>

                                    </div>
                                </div>
                            </div>
                            <table className="table ">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Price</th>
                                        <th>Rating</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                {this.props.products.map((current) => {
                                    return <ProductEntry
                                        key={current.id}
                                        id={current.id}
                                        title={current.title}
                                        category={current.category}
                                        price={current.price}
                                        description={current.description}
                                        image={current.image}
                                        rating={current.rating["rate"]}
                                        count={current.rating["count"]}
                                        removeProduct={this.props.removeProduct}
                                        updateProduct={this.props.updateProduct} />
                                })}
                            </table>

                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Admin;