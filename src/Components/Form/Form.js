import React from 'react';
import validator from 'validator';
import "./Form.css"

class Form extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isValid: false,
            anyError: false,
            errorMsg: "",
            isTitleValid: false,
            isCategoryValid: false,
            isDescriptionValid: false,
            isPriceValid: false,
            isUrlValid: false,
        }
    }

    update = () => {
        this.setState({
            isTitleValid: true,
            isCategoryValid: true,
            isDescriptionValid: true,
            isPriceValid: true,
            isUrlValid: true,
        })
    }
    handelUpdate = (event) => {
        let { isTitleValid,
            isCategoryValid,
            isDescriptionValid,
            isPriceValid,
            isUrlValid } = this.state;

        if (isTitleValid &&
            isCategoryValid &&
            isDescriptionValid &&
            isPriceValid &&
            isUrlValid) {
            let productData = {
                id: this.props.id,
                title: event.target[0].value,
                category: event.target[1].value,
                description: event.target[2].value,
                price: event.target[3].value,
                image: event.target[4].value,
                count: this.props.count,
                rating: this.props.rating,
            }
            this.setState({
                anyError: false,
                isValid: true,
            }, () => {
                this.props.updateProduct(productData);
                this.props.handelEdit();
            })
        } else {
            this.setState({
                anyError: true,
            })
        }


    }

    handelAdd = (event) => {
        let { isTitleValid,
            isCategoryValid,
            isDescriptionValid,
            isPriceValid,
            isUrlValid } = this.state;

        if (isTitleValid &&
            isCategoryValid &&
            isDescriptionValid &&
            isPriceValid &&
            isUrlValid) {
            let productData = {
                title: event.target[0].value,
                category: event.target[1].value,
                description: event.target[2].value,
                price: event.target[3].value,
                image: event.target[4].value,
            }
            this.setState({
                anyError: false,
                isValid: true,
            }, () => {
                this.props.addNewProduct(productData);
            })
        } else {
            this.setState({
                anyError: true,
            })
        }
    }

    handleForm = (event) => {
        event.preventDefault();
        if (this.props.isUpdate === true) {
            this.handelUpdate(event);
        } else {
            this.handelAdd(event);
        }

    }

    handelTitle = (event) => {
        console.log(event.target.value)
        if (event.target.id === "title" && event.target.value.length >= 5) {
            this.setState({
                isTitleValid: true
            })

        } else {
            this.setState({
                isTitleValid: false
            })

        }
    }

    handelCategory = (event) => {
        if (event.target.id === "category" && event.target.value.length >= 5) {
            this.setState({
                isCategoryValid: true,
            })
        } else {
            this.setState({
                isCategoryValid: false,
            })
        }
    }

    handelDescription = (event) => {
        if (event.target.id === "description" && event.target.value.length >= 10) {
            this.setState({
                isDescriptionValid: true,
            })
        } else {
            this.setState({
                isDescriptionValid: false,
            })
        }
    }

    handelPrice = (event) => {
        if (validator.isInt(event.target.value) === true) {
            this.setState({
                isPriceValid: true,
            })
        } else {
            this.setState({
                isPriceValid: false,
            })
        }
    }

    handelImageUrl = (event) => {
        if (validator.isURL(event.target.value) === true) {
            this.setState({
                isUrlValid: true
            })
        } else {
            this.setState({
                isUrlValid: false
            })
        }
    }

    componentDidMount() {
        if (this.props.isUpdate === true) {
            this.update();

        }
    }

    render() {
        return (
            <>
                <form className="form-example" onSubmit={this.handleForm}>

                    {this.props.isUpdate === true ?
                        <h1>Update Product</h1 > :
                        <h1>Add a new product</h1>}

                    {this.state.isValid === true &&
                        <p className='green'>The product added Successfully.</p>}

                    {this.state.anyError === true &&
                        <p className='red'>Please check all details</p>}

                    <div className="form-group">
                        <label
                            htmlFor="title">
                            Product Title
                        </label>
                        <input
                            type="text"
                            className="form-control username"
                            id="title"
                            placeholder="Enter product name"
                            defaultValue={this.props.title}
                            onBlur={this.handelTitle}
                        />
                        {this.state.isTitleValid === false &&
                            <p className='red'>Product Name should be minimum 5 character long.</p>}

                        {this.state.isTitleValid === true &&
                            <p className='green'>Valid</p>}
                    </div>

                    <div className="form-group">
                        <label
                            htmlFor="category">
                            Product Category
                        </label>
                        <input
                            type="text"
                            className="form-control password"
                            id="category"
                            placeholder="Enter the product category"
                            defaultValue={this.props.category}
                            onBlur={this.handelCategory}
                        />
                        {this.state.isCategoryValid === false &&
                            <p className='red'>Product Name should be minimum 5 character long.</p>}

                        {this.state.isCategoryValid === true &&
                            <p className='green'>valid</p>}
                    </div>
                    <div className="form-group">
                        <label htmlFor="description">
                            Product description
                        </label>
                        <textarea
                            className="form-control"
                            id="description"
                            placeholder='Enter the product description'
                            defaultValue={this.props.description}
                            onBlur={this.handelDescription}
                            rows="3" />
                        {this.state.isDescriptionValid === false &&
                            <p className='red'>Product description should be minimum 15 character long.</p>}

                        {this.state.isDescriptionValid === true &&
                            <p className='green'>valid</p>}

                    </div>
                    <div className="form-group">
                        <label
                            htmlFor="price">
                            Product Price
                        </label>
                        <input
                            type="text"
                            className="form-control password"
                            id="price"
                            placeholder="Enter the product Price"
                            defaultValue={this.props.price}
                            onBlur={this.handelPrice}
                        />
                        {this.state.isPriceValid === false &&
                            <p className='red'>Product price should be a integer value</p>}

                        {this.state.isPriceValid === true &&
                            <p className='green'>valid</p>}
                    </div>

                    <div className="form-group">
                        <label
                            htmlFor="image">
                            Product Image Link
                        </label>
                        <input
                            type="text"
                            className="form-control password"
                            id="image"
                            placeholder="Enter the product image link"
                            defaultValue={this.props.image}
                            onBlur={this.handelImageUrl}
                        />
                        {this.state.isUrlValid === false &&
                            <p className='red'>Enter a image url in valid format</p>}

                        {this.state.isUrlValid === true &&
                            <p className='green'>valid</p>}
                    </div>
                    <button
                        type="submit"
                        className="btn-addNew mt-2">
                        {this.props.isUpdate === true ?
                            <span>Update Product</span> : <span>Add Product</span>}

                    </button>


                </form>
            </>
        )
    }
}
export default Form;