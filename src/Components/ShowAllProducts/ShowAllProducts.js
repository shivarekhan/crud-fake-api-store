import React from "react";
import Card from "../Card/Card";
import Loader from "../Loader/Loader.js";
import "./ShowAllProducts.css"

class ShowAllProducts extends React.Component {
    render() {
        return (
            <>
                {this.props.status === "loading" &&
                    <Loader />}

                {this.props.status === "loaded" &&
                    this.props.products.length === 0 &&
                    <h1>No Products to show</h1>}

                {this.props.status === "loaded" &&
                    this.props.products.length > 0 &&

                    <div className="d-flex container mt-5 flex-wrap justify-content-evenly">
                        {this.props.products.map((current) => {
                            return <Card
                                key={current.id}
                                image={current.image}
                                title={current.title}
                                price={current.price}
                                rating={current.rating["rate"]}
                                count={current.rating["count"]}
                                description={current.description}
                            />
                        })}


                    </div>
                }
            </>
        )
    }
}

export default ShowAllProducts;