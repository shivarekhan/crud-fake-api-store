import React from "react";
import "./Card.css"

class Card extends React.Component {
    render() {
        return (
            <>
                <div className="card p-3 bg-white Card-custom mb-3 d-flex flex-column justify-content-between">
                    <div className="about-product text-center mt-2">
                        <img className="custom-card-image p-4" src={this.props.image} />
                        <div>
                            <h5>{this.props.title}</h5>
                        </div>
                    </div>
                    <div className="content"><p>{this.props.description}</p></div>
                    <div className="stats mt-2  align-items--baseline ">
                        <div className="d-flex justify-content-between align-items-end p-price bolder">
                            <span>Rating</span>
                            <span>{this.props.rating}({this.props.count})</span>
                        </div>
                        <div className="d-flex justify-content-between align-items-end p-price bolder">
                            <span>Price</span>
                            <span>${this.props.price}</span>
                        </div>
                    </div>

                </div>
            </>
        )
    }
}

export default Card;