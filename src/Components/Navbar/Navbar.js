import React from "react";
import { Link } from "react-router-dom";
import "./Navbar.css";

class Navbar extends React.Component {
    render() {
        return (
            <>
                <nav className="navbar navbar-light bg-color nav-parent">
                    <div className="nav-logo">
                        <Link to="/">
                            <i className="fa-sharp fa-solid fa-house white-color fa-2x "></i>
                        </Link >
                    </div>

                    <h4 className=" admin">
                        <Link to="/admin">
                            <i className="fa-solid fa-pen-to-square white-color"></i>
                        </Link>
                    </h4>

                </nav>
            </>
        )
    }
}

export default Navbar