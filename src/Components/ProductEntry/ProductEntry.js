import React from 'react';
import { Link } from 'react-router-dom';
import "./ProductEntry.css"
import Form from '../Form/Form';

class ProductEntry extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            isDeleted: false,
            isEdited: false,
        }
    }
    handelCancelDelete = () => {
        this.setState({
            isDeleted: !this.state.isDeleted,
        })
    }
    handelDelete = () => {
        if (this.state.isEdited === true) {
            this.setState({
                isEdited: !this.state.isEdited,
            })
        }
        this.setState({
            isDeleted: !this.state.isDeleted,
        })
    }
    deleteProduct = () => {
        this.props.removeProduct(this.props.id)

    }
    handelEdit = () => {
        if (this.state.isDeleted === true) {
            this.setState({
                isDeleted: !this.state.isDeleted,
            })
        }
        this.setState({
            isEdited: !this.state.isEdited,
        })

    }
    handleForm = (event) => {
        event.preventDefault();

    }
    render() {
        return (
            <>
                <tbody className='full-width'>
                    <tr>
                        <td>{this.props.id}</td>
                        <td>{this.props.title}</td>
                        <td>{this.props.category}</td>
                        <td>${this.props.price}</td>
                        <td>{this.props.rating}<span>({this.props.count})</span></td>
                        <td>
                            <button
                                className='edit-btn'
                                onClick={this.handelEdit}>
                                Edit
                            </button>
                            <button
                                className='delete-btn'
                                onClick={this.handelDelete}>
                                Delete
                            </button>
                        </td>
                    </tr>
                </tbody>
                {this.state.isEdited && <>
                    <tbody className='full-width'>
                        <tr>
                            <td></td>
                            <td>
                                <Form
                                    isUpdate={true}
                                    id={this.props.id}
                                    title={this.props.title}
                                    category={this.props.category}
                                    description={this.props.description}
                                    price={this.props.price}
                                    image={this.props.image}
                                    rating={this.props.rating}
                                    count={this.props.count}
                                    updateProduct={this.props.updateProduct}
                                    handelEdit={this.handelEdit} />
                            </td>
                        </tr>
                    </tbody>
                </>}
                {this.state.isDeleted && <>
                    <tbody className='full-width'>
                        <tr>
                            <td></td>
                            <td className='red-font'>
                                Press Confirm button if you sure to delete the above product
                            </td>
                            <td >
                                <button
                                    className='delete-btn'
                                    onClick={this.deleteProduct}>
                                    Confirm
                                </button>

                            </td>
                            <td><button
                                className='cancel-btn'
                                onClick={this.handelCancelDelete}>Cancel</button>

                            </td>

                        </tr>
                    </tbody>
                </>}
            </>
        )
    }
}

export default ProductEntry;