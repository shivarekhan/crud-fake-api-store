import React, { Component } from 'react'
import Form from '../Form/Form'
import "./AddNewProduct.css"

export default class AddNewProduct extends Component {
    render() {
        return (
            <>
                <div className="container h-100 mt-3">
                    <div className="row h-100 justify-content-center align-items-center">
                        <div className="col-10 col-md-8 col-lg-6">
                            <Form
                                isUpdate={false}
                                addNewProduct={this.props.addNewProduct} />
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
